GAME_GENRES = [
    "Action",
    "Action Rogue-Like",
    "Arcade & Rhythm",
    "Beat `Em Up",
    "Fighting & Martial Arts",
    "First-Person Shooter",
    "Platformer & Runner",
    "Third-Person SHooter",

    "Role-Playing",
    "Action RPG",
    "Adventure RPG",
    "JRPG",
    "Party-Based",
    "Rogue-Like",
    "Strategy RPG",
    "Turn-Based",

    "Strategy",
    "Card & Board",
    "City & Settlement",
    "Grand & 4X",
    "Military",
    "Real-Time Strategy (RTS)",
    "Tower Defense",
    "Turn-Based Strategy",

    "Adventure & Casual",
    "Adventure",
    "Adventure RPG",
    "Casual",
    "Metroidvania",
    "Puzzle",
    "Story-Rich",
    "Visual Novel",

    "Simulation",
    "Building & Automation",
    "Business & Tycoon",
    "Dating",
    "Farming & Crafting",
    "Life & Immersive",
    "Sandbox & Physics",
    "Space & Flight",

    "Sports & Racing",
    "All Sports",
    "Fishing & Hunting",
    "Individual Sports",
    "Racing",
    "Racing Sim",
    "Sports Sim",
    "Team Sports",

    "Indie"
]













