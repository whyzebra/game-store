# from game_store_api import setup_game_store_api
# game_store = setup_game_store_api()
# game_store.app_context().push()

from game_store_api.models import db
from game_store_api.models import RoleModel
from game_store_api.models import UserModel
from game_store_api.models import GameModel
from game_store_api.models import GenreModel
from setup.game_genres import GAME_GENRES
from setup.games import GAMES_LIST

from typing import List, Union


def add_roles(*role_titles: str) -> None:
    """
    Adds all non-keyword arguments stored in :role_titles: to the database

    :param role_titles: non-keyword arguments, containing role titles
    """

    for role_title in role_titles:
        if not get_role(role_title):
            role = RoleModel(title=role_title)
            role.save()

            print(f"Role '{role_title}' has been added!")
    print()


def get_role(title: str) -> Union[RoleModel, None]:
    """
    Gets RoleModel object by RoleModel.title field

    :param title: role name
    :return: RoleModel object with specified :role: if it exists, else - None
    """
    return RoleModel.query.filter_by(title=title).first()


def deploy_hanekawa() -> None:
    """
    Adds user with 'Admin' role to database
    """
    hanekawa = UserModel(
        username="hanekawa",
        email="tsubasa.hanekawa@monogatari.com",
        password="Hanekawa@123",
        first_name="Tsubasa",
        last_name="Hanekawa",
    )
    hanekawa.role = get_role('SuperAdmin')
    hanekawa.save()

    print(f"'Hanekawa' has been deployed!\n")


def add_users() -> None:
    """
    Adds 3 test users to database with 'User', 'Manager' and 'Admin' roles
    """
    user = UserModel(
        username="test_user",
        email="test_user@gamestore.com",
        password="User*01",
        first_name="Test",
        last_name="User",
    )
    user.save()
    print(f"'{user.username}' has been added!")

    manager = UserModel(
        username="test_manager",
        email="test_manager@gamestore.com",
        password="Manager*01",
        first_name="Test",
        last_name="Manager",
        role=RoleModel.get_role('Manager')
    )
    manager.save()
    print(f"'{manager.username}' has been added!")

    admin = UserModel(
        username="test_admin",
        email="test_admin@gamestore.com",
        password="Admin*01",
        first_name="Test",
        last_name="Admin",
        role=RoleModel.get_role('Admin')
    )
    admin.save()
    print(f"'{admin.username}' has been added!")
    print()


def get_user(username: str) -> Union[UserModel, None]:
    """
    Gets UserModel object by UserModel.username field

    :param username: username string
    :return: UserModel object with specified :username: if it exists, else - None
    """
    return UserModel.query.filter_by(username=username).first()


def add_genres() -> None:
    """
    Adds all game genres, stored in GAME_GENRES list, to database
    """
    used = {}

    for genre_name in GAME_GENRES:
        if genre_name not in used:
            new_genre = GenreModel(name=genre_name)
            new_genre.save()

            used[genre_name] = True

    print(f'{len(GAME_GENRES)} game genres was successfully added to database!\n')


def get_genre(name) -> GenreModel:
    """
    Gets GenreModel object by GenreModel.name field

    :param name: name string
    :return: GenreModel object with specified :name: if it exists, else - None
    """
    return GenreModel.query.filter_by(name=name).first()


def set_game_genres(game_genres: List[str]) -> List[GenreModel]:
    """
    Converts list of game genres strings to GenreModel objects list

    :param game_genres: list of strings, representing game genres
    :return: list of GenreModel objects with appropriate game genres, specified in :game_genres:
    """
    genres = []

    for genre_name in game_genres:
        genre = get_genre(genre_name)

        if genre:
            genres.append(genre)

    return genres


def add_games() -> None:
    """
    Add all games, stored in GAMES_LIST, to database.

    Sets user 'hanekawa' as manager to all games.
    """
    manager = get_user(username='hanekawa')

    for game in GAMES_LIST:
        new_game = GameModel(
            title=game.get('title'),
            genres=set_game_genres(game['genres']),
            developer=game.get('developer'),
            publisher=game.get('publisher'),
            description=game.get('description'),
            price=game.get('price')
        )

        new_game.managed_by = manager
        new_game.save()

    print(f'{len(GAMES_LIST)} games was successfully added to database!')


def setup_test_database():
    print('Running db.drop_all()...')
    db.drop_all()

    print('Running db.create_all()...')
    db.create_all()

    print("Adding basic roles: 'User', 'Manager', 'Admin'")
    add_roles('User', 'Manager', 'Admin', 'SuperAdmin')

    print("Adding default users...")
    add_users()

    print("Deploying 'Hanekawa'...")
    deploy_hanekawa()

    print("Adding game genres...")
    add_genres()

    print("Adding games...")
    add_games()


if __name__ == '__main__':
    from game_store_api import setup_development_game_store_api
    game_store = setup_development_game_store_api()
    game_store.app_context().push()

    print('Running db.drop_all()...')
    db.drop_all()
    print('Running db.create_all()...')
    db.create_all()

    print("Adding basic roles: 'User', 'Manager', 'Admin'")
    add_roles('User', 'Manager', 'Admin', 'SuperAdmin')

    print("Adding default users...")
    add_users()

    print("Deploying 'Hanekawa'...")
    deploy_hanekawa()

    print("Adding game genres...")
    add_genres()

    print("Adding games...")
    add_games()
