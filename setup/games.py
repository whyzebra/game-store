# ========= Action =========
CSGO = {
    'title': 'Counter-Strike: Global Offensive',
    'genres': ['Action', 'First-Person Shooter'],
    'developer': 'Valve, Hidden Path Entertainment',
    'publisher': 'Valve',
    'description': """Counter-Strike: Global Offensive (CS: GO) expands upon the team-based action gameplay
that it pioneered when it was launched 19 years ago.

CS: GO features new maps, characters, weapons, and game modes, and delivers updated
versions of the classic CS content (de_dust2, etc.).

"Counter-Strike took the gaming industry by surprise when the unlikely MOD became the
most played online PC action game in the world almost immediately after its release in August
1999," said Doug Lombardi at Valve. "For the past 12 years, it has continued to be one of the
most-played games in the world, headline competitive gaming tournaments and selling over
25 million units worldwide across the franchise. CS: GO promises to expand on CS' award-
winning gameplay and deliver it to gamers on the PC as well as the next gen consoles and the Mac." 
""",
    'price': 15
}

DAYS_GONE = {
    'title': 'Days Gone',
    'genres': ['Action', 'Adventure', 'Story-Rich'],
    'developer': 'Bend Studio',
    'publisher': 'PlayStation Mobile, Inc.',
    'description': """Days Gone is an open-world action-adventure game set in a harsh wilderness two years after 
a devastating global pandemic.

Step into the dirt flecked shoes of former outlaw biker Deacon St. John, a bounty hunter 
trying to find a reason to live in a land surrounded by death. Scavenge through abandoned 
settlements for equipment to craft valuable items and weapons, or take your chances with 
other survivors trying to eke out a living through fair trade… or more violent means.

KEY FEATURES
• A striking setting: From forests and meadows, to snowy plains and desert lava fields, the 
Pacific Northwest is both beautiful and lethal. Explore a variety of mountains, caves, mines 
and small rural towns, scarred by millions of years of volcanic activity.
• Brutal encounters: With vicious gangs and hordes of Freakers roaming the land, you’ll need 
to make full use of a variety of customizable traps, weapons, and upgradable skills to stay 
alive. Don’t forget your Drifter bike, an invaluable tool in a vast land.
• An ever-changing environment: Jump on the saddle of Deacon’s trusty motorbike and 
explore a dynamic world dramatically affected by the weather, a dramatic day/night cycle and 
the evolving Freakers, who adjust to their surroundings – and the people in it.
• A compelling story: Lose yourself in a powerful tale of desperation, betrayal and regret, as 
Deacon St. John searches for hope after suffering a deep, personal loss. What makes us 
human when faced with the daily struggle for survival?
""",
    'price': 35
}

RESIDENT_EVIL_VILLAGE = {
    'title': 'RESIDENT EVIL VILLAGE',
    'genres': ['Action'],
    'developer': 'CAPCOM Co., Ltd.',
    'publisher': 'CAPCOM Co., Ltd.',
    'description': """Experience survival horror like never before in the eighth major installment in the storied 
Resident Evil franchise - Resident Evil Village.

Set a few years after the horrifying events in the critically acclaimed Resident Evil 7 biohazard, 
the all-new storyline begins with Ethan Winters and his wife Mia living peacefully in a new 
location, free from their past nightmares. Just as they are building their new life together, 
tragedy befalls them once again.

First-Person Action – Players will assume the role of Ethan Winters and experience every 
up-close battle and terrifying pursuit through a first-person perspective.
Familiar Faces and New Foes – Chris Redfield has typically been a hero in the Resident Evil 
series, but his appearance in Resident Evil Village seemingly shrouds him in sinister 
motives. A host of new adversaries inhabiting the village will relentlessly hunt Ethan and 
hinder his every move as he attempts to make sense of the new nightmare he finds himself in.
""",
    'price': 50
}


# ========= Role-Playing =========
FINAL_FANTASY_XIV_Online = {
    'title': 'FINAL FANTASY XIV Online',
    'genres': ['Role-Playing', 'Massively Multiplayer', 'JRPG'],
    'developer': 'Square Enix',
    'publisher': 'Square Enix',
    'description': """Take part in an epic and ever-changing FINAL FANTASY as you adventure and explore with 
friends from around the world.'

Join over 22 million adventurers worldwide on an adventure that will take you 
to the heavens and beyond!
""",
    'price': 10,
}

MASS_EFFECT_LEGENDARY = {
    'title': 'Mass Effect™ Legendary Edition',
    'genres': ['Action', 'Role-Playing'],
    'developer': 'BioWare',
    'publisher': 'Electronic Arts',
    'description': """One person is all that stands between humanity and the greatest threat it’s ever faced. Relive 
the legend of Commander Shepard in the highly acclaimed Mass Effect trilogy with the Mass 
Effect™ Legendary Edition. Includes single-player base content and over 40 DLC from Mass 
Effect, Mass Effect 2, and Mass Effect 3 games, including promo weapons, armors and packs 
— remastered and optimized for 4K Ultra HD.

Experience an amazingly rich and detailed universe where your decisions have profound 
consequences on the action and the outcome.
""",
    'price': 65
}

TES_ONLINE = {
    'title': 'The Elder Scrolls® Online',
    'genres': ['Massively Multiplayer', 'Role-Playing'],
    'developer': 'Zenimax Online Studios',
    'publisher': 'Bethesda Softworks',
    'description': """Experience an ever-expanding story across all of Tamriel in The Elder Scrolls Online, an 
award-winning online RPG. Explore a rich, living world with friends or embark upon a solo 
adventure. Enjoy complete control over how your character looks and plays, from the 
weapons you wield to the skills you learn – the choices you make will shape your destiny. 
Welcome to a world without limits.
""",
    'price': 20
}


# ========= Strategy =========
CITIES_SKYLINES = {
    'title': 'Cities: Skylines',
    'genres': ['Simulation', 'Strategy', 'Building & Automation', 'Life & Immersive'],
    'developer': 'Colossal Order Ltd.',
    'publisher': 'Paradox Interactive',
    'description': """Cities: Skylines is a modern take on the classic city simulation. The game introduces new 
game play elements to realize the thrill and hardships of creating and maintaining a real city 
whilst expanding on some well-established tropes of the city building experience. You’re only 
limited by your imagination, so take control and reach for the sky!
""",
    'price': 15
}

BEFORE_WE_LEAVE = {
    'title': 'Before We Leave',
    'genres': ['Casual', 'Indie', 'Simulation', 'Strategy'],
    'developer': 'Team17',
    'publisher': 'Team17',
    'description': """Rediscover what was lost and nurture your reborn civilisation in Before We Leave, a non-violent 
city building game set in a cozy corner of the universe. Grow, gather and manage 
resources to help your settlements thrive, and share goods between the hexagonal lands and 
planets around you. Relax and expand the fabric of your growing societies and create a solar 
system of happy planets at your own pace. Not everything will go your way, though - use your 
wits and research solutions to overcome the challenges that your ancestors once faced.
""",
    'price': 10
}

FARM_MANAGER_2021 = {
    'title': 'Farm Manager 2021',
    'genres': ['Indie', 'Simulation', 'Strategy', 'Life & Immersive'],
    'developer': 'PlayWay S.A., Sim Farm S.A.',
    'publisher': 'PlayWay S.A., Sim Farm S.A.',
    'description': """Get ready for a logistic challenge in the next part of the best-selling series, which has been 
sold in over 120,000 copies! In Farm Manager 2021, as a farm manager, you have to ensure 
the right level of harvest, employee satisfaction, animal health, equipment efficiency and 
proper crop processing. You can buy new plots and use materials from abandoned buildings to 
expand your farm. Thanks to the new, more intuitive interface, employee management is even simpler.
""",
    'price': 10
}


# ========= Adventure & Casual =========
IT_TAKES_TWO = {
    'title': 'It Takes Two',
    'genres': ['Adventure & Casual', 'Action', 'Adventure'],
    'developer': 'Hazelight',
    'publisher': 'Electronic Arts',
    'description': """Embark on the craziest journey of your life in It Takes Two, a genre-bending platform 
adventure created purely for co-op. Invite a friend to join for free with Friend’s Pass and work 
together across a huge variety of gleefully disruptive gameplay challenges. Play as the 
clashing couple Cody and May, two humans turned into dolls by a magic spell. Together, 
trapped in a fantastical world where the unpredictable hides around every corner, they are 
reluctantly challenged with saving their fractured relationship.
""",
    'price': 40
}

SUBNAUTICA_BELOW_ZERO = {
    'title': 'Subnautica: Below Zero',
    'genres': ['Adventure & Casual', 'Adventure', 'Indie'],
    'developer': 'Unknown Worlds Entertainment',
    'publisher': 'Unknown Worlds Entertainment',
    'description': """Below Zero is an underwater adventure game set on an alien ocean world. It is a new chapter 
in the Subnautica universe, and is developed by Unknown Worlds.

Submerge yourself in an all-new, sub-zero expedition in an arctic region of Planet 4546B. 
Arriving with little more than your wits and some survival equipment, you set out to 
investigate what happened to your sister...
""",
    'price': 15
}

RDR_2 = {
    'title': 'Red Dead Redemption 2',
    'genres': ['Adventure & Casual', 'Action', 'Adventure', 'Story-Rich'],
    'developer': 'Rockstar Games',
    'publisher': 'Rockstar Games',
    'description': """America, 1899.

Arthur Morgan and the Van der Linde gang are outlaws on the run. With federal agents and 
the best bounty hunters in the nation massing on their heels, the gang must rob, steal and 
fight their way across the rugged heartland of America in order to survive. As deepening 
internal divisions threaten to tear the gang apart, Arthur must make a choice between his own 
ideals and loyalty to the gang who raised him.
""",
    'price': 30
}


# ========= Simulation =========
SNOW_RUNNER = {
    'title': 'SnowRunner',
    'genres': ['Adventure', 'Simulation'],
    'developer': 'Saber Interactive',
    'publisher': 'Saber Interactive',
    'description': """Get ready for the next-generation off-road experience!

SnowRunner puts you in the driver’s seat of powerful vehicles as you conquer extreme open 
environments with the most advanced terrain simulation ever. Drive 40 vehicles from brands 
such as Ford, Chevrolet, and Freightliner as you leave your mark on an untamed open world.

Overcome mud, torrential waters, snow, and frozen lakes while taking on perilous contracts 
and missions. Expand and customize your fleet with many upgrades and accessories including 
an exhaust snorkel for heavy waters or chain tires to battle the snow.

Ride solo or with other players in 4-player co-operative and expand your SnowRunner 
experience with community-created mods!
• Face extreme environments in a highly advanced physics engine
• 40 unique vehicles to unlock, upgrade, and customize
• Complete dozens of challenging missions across an interconnected world
• Go solo or play with other players in 4-player co-op.
""",
    'price': 25
}

STELLARIS = {
    'title': 'Stellaris',
    'genres': ['Simulation', 'Strategy', 'Space & Flight'],
    'developer': 'Paradox Development Studio',
    'publisher': 'Paradox Interactive',
    'description': """Get ready to explore, discover and interact with a multitude of species as you journey among 
the stars. Forge a galactic empire by sending out science ships to survey and explore, while 
construction ships build stations around newly discovered planets. Discover buried treasures 
and galactic wonders as you spin a direction for your society, creating limitations and 
evolutions for your explorers. Alliances will form and wars will be declared.

Like all our Grand Strategy games, the adventure evolves with time. Because free updates are 
a part of any active Paradox game, you can continue to grow and expand your empire with 
new technologies and capabilities. What will you find beyond the stars? Only you can answer that.
""",
    'price': 20
}


SIMS4 = {
    'title': 'The Sims™ 4',
    'genres': ['Casual', 'Simulation'],
    'developer': 'Maxis',
    'publisher': 'Electronic Arts',
    'description': """Unleash your imagination and create a world of Sims that’s wholly unique. Explore and 
    customize every detail, from Sims to homes and much more. Choose how Sims look, act, and 
    dress. Determine how they’ll live out each day.

Design and build incredible homes for every family, then decorate with your favorite 
furnishings and décor. Travel to different neighborhoods where you can meet other Sims and 
learn about their lives.
""",
    'price': 40
}


# ========= Sports & Racing =========
FIFA_21 = {
    'title': 'EA SPORTS™ FIFA 21',
    'genres': ['Sports & Racing', 'Simulation', 'Sports'],
    'developer': 'Electronic Arts',
    'publisher': 'Electronic Arts',
    'description': """What is FIFA?

Play The World's Game with 17,000+ players, over 700 teams in 90+ stadiums, and more than 
30 leagues from all over the globe.

Give us all your money for cheap 'reskin' of a game about ball.
""",
    'price': 60
}

FORZA_HORIZON_4 = {
    'title': 'Forza Horizon 4',
    'genres': ['Sports & Racing','Racing'],
    'developer': 'Playground Games',
    'publisher': 'Xbox Game Studios',
    'description': """Dynamic seasons change everything at the world’s greatest automotive festival. Go it alone or 
team up with others to explore beautiful and historic Britain in a shared open world. Collect, 
modify and drive over 450 cars. Race, stunt, create and explore – choose your own path to 
become a Horizon Superstar.
""",
    'price': 30
}

ASSETO_CORSA = {
    'title': 'Assetto Corsa',
    'genres': ['Sports & Racing', 'Indie', 'Racing', 'Simulation', 'Sports', 'Racing Sim'],
    'developer': 'Kunos Simulazioni',
    'publisher': 'Kunos Simulazioni',
    'description': """NEXT GENERATION RACING SIMULATOR
Assetto Corsa features an advanced DirectX 11 graphics engine that recreates an immersive 
environment, dynamic lighting and realistic materials and surfaces. The advanced physics 
engine is being designed to provide a very realistic driving experience, including features and 
aspects of real cars, never seen on any other racing simulator such as tyre flat spots, heat 
cycles including graining and blistering, very advanced aerodynamic simulation with active 
movable aerodynamics parts controlled in real time by telemetry input channels, hybrid 
systems with kers and energy recovery simulation. Extremely detailed with single player and 
multiplayer options, exclusive licensed cars reproduced with the best accuracy possible, 
thanks to the official cooperation of Car Manufacturers.
ASSETTO CORSA has been developed at the KUNOS Simulazioni R&D office, located just 
inside the international racing circuit of Vallelunga, allowing the team to develop the game 
with the cooperation of real world racing drivers and racing teams.
""",
    'price': 10
}

GAMES_LIST = [
    CSGO,
    DAYS_GONE,
    RESIDENT_EVIL_VILLAGE,

    FINAL_FANTASY_XIV_Online,
    MASS_EFFECT_LEGENDARY,
    TES_ONLINE,

    CITIES_SKYLINES,
    BEFORE_WE_LEAVE,
    FARM_MANAGER_2021,

    IT_TAKES_TWO,
    SUBNAUTICA_BELOW_ZERO,
    RDR_2,

    SNOW_RUNNER,
    STELLARIS,
    SIMS4,

    FIFA_21,
    FORZA_HORIZON_4,
    ASSETO_CORSA
]















