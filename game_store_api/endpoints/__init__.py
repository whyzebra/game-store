def register_all_endpoints(api):
    # Auth resources
    from game_store_api.resources import UserRegistrationResource
    from game_store_api.resources import UserLoginResource
    from game_store_api.resources import UserLogoutResource

    # JWT tokens management resources
    from game_store_api.resources import RefreshAccessTokenResource
    from game_store_api.resources import RevokeAccessTokenResource
    from game_store_api.resources import RevokeRefreshTokenResource

    # Public Game resources
    from game_store_api.resources import GameResource
    from game_store_api.resources import GamesResource

    # Manage Game resources
    from game_store_api.resources import HideGameResource
    from game_store_api.resources import UnhideGameResource

    # Test resources
    from game_store_api.resources import PlaceholderResource
    from game_store_api.resources import TestProtectedResource

    # User authentication and authorization
    api.add_resource(UserRegistrationResource, '/api/v1/register')
    api.add_resource(UserLoginResource, '/api/v1/login')
    api.add_resource(UserLogoutResource, '/api/v1/logout')

    # JWT tokens management
    api.add_resource(RefreshAccessTokenResource, '/api/v1/refresh_access_token')
    api.add_resource(RevokeAccessTokenResource, '/api/v1/revoke_access_token')
    api.add_resource(RevokeRefreshTokenResource, '/api/v1/revoke_refresh_token')

    # Public Games resources, available for guests and all User roles
    api.add_resource(GameResource, '/api/v1/game/<string:game_id>')
    api.add_resource(GamesResource, '/api/v1/games')

    # Manage Games resources, available for game direct managers, admins and superadmins
    api.add_resource(HideGameResource, '/api/v1/game/manage/hide/<string:game_id>')
    api.add_resource(UnhideGameResource, '/api/v1/game/manage/unhide/<string:game_id>')

    # Some test (temp) resources
    api.add_resource(PlaceholderResource, '/', '/api/v1/')
    api.add_resource(TestProtectedResource, '/api/v1/test_protected/<string:game_id>/')
