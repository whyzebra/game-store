from flask_jwt_extended import decode_token
# from game_store_api.redis_instances import REDIS_BLOCKLIST
from datetime import datetime
import redis
from game_store_api.redis_db import RedisDB


class JWTBlockList:
    """
    JWT Tokens "Block List" realization

    This class helps to handle Logout and JWT tokens invalidation/revoking process
    """
    REDIS_DB = RedisDB.REDIS_BLOCKLIST
    # REDIS_DB = REDIS_BLOCKLIST

    @classmethod
    def add_token(cls, jwt_payload: dict) -> None:
        """
        Adds "jti" as key to block list and sets its value as "exp" time
        key - "jti" (unique identifier for the JWT)
        value - "exp" time (represented as timestamp string)

        :param jwt_payload: decoded JWT token
        :return: None
        """
        cls.REDIS_DB.setex(
            name=jwt_payload.get('jti'),
            time=cls.get_token_exp_time(jwt_payload),
            value=jwt_payload.get('exp')
        )

    @classmethod
    def is_token_blocked(cls, jwt_payload: dict) -> bool:
        """
        Check whether :jwt_token: is present in block list

        :param jwt_payload: decoded JWT token
        :return: True if :jwt_token: is blocked, else False
        """
        return bool(cls.REDIS_DB.exists(jwt_payload.get('jti')))

    @classmethod
    def get_token_exp_time(cls, jwt_payload: dict) -> int:
        """
        Calculates JWT token expiration time in seconds

        :param jwt_payload: decoded JWT token
        :return: token expiration time in seconds
        """
        token_exp_at = datetime.fromtimestamp(jwt_payload.get('exp'))
        token_exp_time = (token_exp_at - datetime.now()).seconds

        return token_exp_time
