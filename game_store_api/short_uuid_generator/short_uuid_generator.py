# from game_store_api.redis_instances import REDIS_USED_SHORT_UUIDS
from game_store_api.redis_db import RedisDB
import uuid


def generate_short_uuid() -> str:
    """
    Generates unique short uuid (5 characters long).

    This method guarantees uniqueness and avoid any collisions
    by storing all used short uuids in the redis db

    :return: unique uuid id
    """
    length = 5

    while True:
        short_uuid = str(uuid.uuid4())[:length]

        if not RedisDB.REDIS_USED_SHORT_UUIDS.exists(short_uuid):
            RedisDB.REDIS_USED_SHORT_UUIDS.set(short_uuid, 1)
            break

        RedisDB.REDIS_USED_SHORT_UUIDS.incr('collisions', 1)

    return short_uuid
