import os
from dotenv import load_dotenv
from datetime import timedelta


load_dotenv()


class BasicConfig:
    DEBUG = True
    SECRET_KEY = os.getenv('GAME_STORE_SECRET')
    JWT_SECRET_KEY = os.getenv('JWT_SECRET')
    JWT_ACCESS_TOKEN_EXPIRES = timedelta(minutes=15)
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class DevelopmentConfig(BasicConfig):
    SQLALCHEMY_DATABASE_URI = os.getenv('DEVELOPMENT_DATABASE_URI', 'sqlite:///default_development.db')


class TestConfig(BasicConfig):
    SQLALCHEMY_DATABASE_URI = os.getenv('TEST_DATABASE_URI', 'sqlite:///default_test.db')
