from game_store_api.marshmallow_schemas import ma
from game_store_api.models import UserModel
from marshmallow.validate import Length, Range, Email


class UserDataSchema(ma.SQLAlchemySchema):
    class Meta:
        fields = ('role_title', 'username', 'email', 'first_name', 'last_name')
        ordered = True

    username = ma.Str()
    role_title = ma.Str()
    email = ma.Str()
    first_name = ma.Str()
    last_name = ma.Str()


class UserRegistrationSchema(ma.SQLAlchemySchema):
    class Meta:
        fields = ('username', 'email', 'password', 'first_name', 'last_name')
        ordered = True

    username = ma.Str(required=True, validate=Length(min=4, max=20))
    email = ma.Str(required=True, validate=Email())
    password = ma.Str(required=True)
    first_name = ma.Str(required=True, validate=Length(min=2, max=20))
    last_name = ma.Str(required=True, validate=Length(min=2, max=20))


class UserRegistrationResponseSchema(UserRegistrationSchema):
    class Meta:
        exclude = ('password',)
        ordered = True

    access_token = ma.Str()
    refresh_token = ma.Str()


class UserLoginSchema(ma.SQLAlchemySchema):
    username = ma.Str(required=True)
    password = ma.Str(required=True)


class UserLoginResponseSchema(UserDataSchema):
    class Meta:
        ordered = True

    access_token = ma.Str()
    refresh_token = ma.Str()
