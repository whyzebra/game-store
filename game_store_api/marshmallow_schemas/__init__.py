from flask_marshmallow import Marshmallow
ma = Marshmallow()


from .user_schema import UserDataSchema
from .user_schema import UserRegistrationSchema
from .user_schema import UserRegistrationResponseSchema
from .user_schema import UserLoginSchema
from .user_schema import UserLoginResponseSchema

from .game_schemas import GameDetailedDataSchema
from .game_schemas import GamesQueryParamsValidationSchema
from .game_schemas import GamesPaginatedListSchema

from .game_schemas import GameBriefDataSchema


user_data_schema = UserDataSchema()
user_registration_schema = UserRegistrationSchema()
user_registration_response_schema = UserRegistrationResponseSchema()
user_login_schema = UserLoginSchema()
user_login_response_schema = UserLoginResponseSchema()

game_detailed_data_schema = GameDetailedDataSchema()
games_query_params_validation_schema = GamesQueryParamsValidationSchema()
game_brief_data_schema = GameBriefDataSchema()
games_paginated_list_schema = GamesPaginatedListSchema()
