from game_store_api.marshmallow_schemas import ma

# Marshmallow
from marshmallow import Schema
from marshmallow import fields
from marshmallow import pre_load
from marshmallow import validates_schema
from marshmallow import ValidationError
from marshmallow.validate import Length, Range, OneOf, ContainsOnly

# Models
from game_store_api.models import GenreModel
from game_store_api.models import GameModel

# Typing
from typing import List


class GameBriefDataSchema(Schema):
    class Meta:
        fields = ('title', 'genres', 'price')
        ordered = True

    title = fields.String()
    genres = fields.Method("get_genres")
    price = fields.Method("get_price")

    @staticmethod
    def get_genres(obj: GameModel) -> List[str]:
        """
        Converts list of GenreModel objects to list of GenreModel.name strings

        :param obj: GameModel
        :return: list of game genres names
        """
        genres_list = []

        for genre in obj.genres:
            genres_list.append(genre.name)

        return genres_list

    @staticmethod
    def get_price(obj: GameModel) -> str:
        """
        Converts GameModel.price from float to str with '$' sign

        :param obj: GameModel
        :return: GameModel.price as string with '$' sign
        """
        if int(obj.price) == obj.price:
            return f'{int(obj.price)}$'

        return f'{round(obj.price, 2)}$'


class GameDetailedDataSchema(GameBriefDataSchema):
    class Meta:
        fields = ('title', 'developer', 'publisher', 'genres', 'price', 'description')
        ordered = True

    developer = fields.String()
    publisher = fields.String()
    description = fields.String()


class GameFullDataSchema(GameDetailedDataSchema):
    class Meta:
        fields = (
            'public_id', 'title', 'developer', 'publisher', 'genres',
            'price', 'licenses_num', 'description', 'managed_by')

    public_id = fields.String()
    licenses_num = fields.Integer()
    managed_by = fields.Method('get_game_manager')

    @staticmethod
    def get_game_manager(obj: GameModel) -> str:
        """
        Extracts username, first_name and lastname from game manager and concatenate to 1 string
        (GameModel.managed_by: UserModel)

        :param obj: GameModel
        :return: string of format '{manager.first_name} {manager.last_name} (@{manager.username})'
        """
        manager = obj.managed_by
        return f'{manager.first_name} {manager.last_name} (@{manager.username})'


class GamesPaginatedListSchema(Schema):
    class Meta:
        ordered = True

    current_page = fields.Integer()
    last_page = fields.Integer()
    total_games = fields.Integer()
    items_per_page = fields.Integer()
    games = fields.Dict(keys=fields.String(), values=fields.Nested(GameBriefDataSchema()))


class GamesQueryParamsValidationSchema(Schema):
    current_page = fields.Integer(missing=1, validate=Range(min=1))
    items_per_page = fields.Integer(missing=5, validate=Range(min=1, max=50))
    sort_by = fields.String(missing='title', validate=OneOf(['title', 'price']))
    sort_order = fields.String(missing='ASC', validate=OneOf(['ASC', 'DESC']))
    genres = fields.List(fields.String, validate=ContainsOnly(GenreModel.get_genre_names_list()))
    title_contains = fields.String(missing=None)
    min_price = fields.Float(missing=GameModel.get_min_price(), validate=Range(min=0))
    max_price = fields.Float(missing=GameModel.get_max_price(), validate=Range(min=0))

    @pre_load
    def convert_numeric_fields(self, in_data, **kwargs):
        """
        Converts 'current_page' & 'items_per_page' fields to int and
        'min_price' & 'max_price' fields to float

        :param in_data: data for deserialization
        :return: modified :in_data:
        """
        try:
            in_data['current_page'] = int(in_data['current_page'])
            in_data['items_per_page'] = int(in_data['items_per_page'])
            in_data['min_price'] = float(in_data['min_price'])
            in_data['max_price'] = float(in_data['max_price'])
        except ValueError:
            pass
        except KeyError:
            pass

        return in_data

    @pre_load
    def normalize_string_fields(self, in_data, **kwargs) -> dict:
        """
        Transforms 'sort_by' field to lowercase and 'sort_order' to uppercase

        :param in_data: data for deserialization
        :return: modified :in_data:
        """
        try:
            in_data['sort_by'] = in_data['sort_by'].lower()
            in_data['sort_order'] = in_data['sort_order'].upper()
        except AttributeError:
            pass

        return in_data

    @validates_schema
    def validate_price_range(self, in_data, **kwargs) -> None:
        """
        Raises ValidationError if 'max_price' value is less or equal to 'min_price'

        :param in_data: data for deserialization
        :return: None
        """
        min_price = in_data.get('min_price')
        max_price = in_data.get('max_price')

        if min_price and max_price:
            if max_price <= min_price:
                raise ValidationError("Field 'max_price' must be greater than 'min_price'")
