from flask_restful import Resource
from flask import request
from datetime import datetime


class PlaceholderResource(Resource):
    def get(self):
        return {
            "utc time": datetime.utcnow().strftime("%d-%m-%Y %H:%M:%S"),
            "message": "Hello, I am a Placeholder :)",
            "args": request.args
        }
