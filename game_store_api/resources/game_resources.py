from flask_restful import Resource
from flask import request

# JWT stuff
from flask_jwt_extended import jwt_required
from flask_jwt_extended import get_jwt_identity

# Marshmallow schemas
from game_store_api.marshmallow_schemas import game_detailed_data_schema
from game_store_api.marshmallow_schemas import games_query_params_validation_schema
from game_store_api.marshmallow_schemas import games_paginated_list_schema

# Models
import sqlalchemy
from game_store_api.models import UserModel
from game_store_api.models import GameModel
from game_store_api.models import GenreModel

# Roles management
from game_store_api.common import role_required

# other
import math
from pprint import pprint


# TODO:
#   1 > Add New Game
#       - endpoint: api/v1/game/add
#       - method: POST
#       - description: Adds new GameModel object to database
#   @
#   2 > Delete Game
#       - endpoint: api/v1/game/<string:game_id>/manage/delete
#       - method: DELETE
#       - description: Deletes game from database by 'public_id'
#   @
#   3 > Edit Game 'title', 'description' (optional: 'developer', 'publisher')
#       endpoint: api/v1/game/<string:game_id>/manage/edit
#       method: PUT
#       description: Edits GameModel 'title', 'description', 'developer', 'publisher' fields
#   @
#   4 > Add Genre to Game genres list
#       endpoint: api/v1/game/<string:game_id>/manage/add_genre/<string:genre_name>
#       method: POST
#       description: Adds genre to GameModel.genres; *genre_name: GenreModel.name
#   @
#   5 > Remove Genre from Game genres list
#       endpoint: api/v1/game/<string:game_id>/manage/remove_genre/<string:genre_name>
#       method: DELETE
#       description: Removes genre from GameModel.genres; *genre_name: GenreModel.name
#   @
#   6 > Edit Game Price
#       endpoint: api/v1/game/<string:game_id>/manage/change_price
#       method: PUT
#       description: Edits Game Price
#   @
#   7 > Set total amount of Game licenses
#       endpoint: api/v1/game/<string:game_id>/manage/set_licenses_num
#       method: PUT
#       description: sets GameModel.licenses_num to new value
#   @
#   8 > Assign new Manager to the Game
#       endpoint: api/v1/game/<string:game_id>/manage/assign_manager
#       method: PUT
#       description: sets GameModel.managed_by field to UserModel with 'Manager' or 'Admin' role


class GameResource(Resource):
    def get(self, game_id):
        game = GameModel.get_game(game_id)

        if not game or game.is_hidden:
            return {'message': 'Game does not exist.'}, 404

        return game_detailed_data_schema.dump(game)


class GamesResource(Resource):
    @jwt_required(optional=True)
    def get(self):
        auth_user = get_jwt_identity()
        current_user = UserModel.get_user(username=auth_user)

        query_params = dict(request.args)
        query_params['genres'] = request.args.getlist('genres')

        errors = games_query_params_validation_schema.validate(query_params)
        if errors:
            response = {
                'message': 'Query query_params validation failed!',
                'errors': errors
            }
            return response, 400

        query_params = games_query_params_validation_schema.load(query_params)

        SORT_BY = GameModel.title
        if query_params['sort_by'] == 'price':
            SORT_BY = GameModel.price

        SORT_ORDER = SORT_BY.asc()
        if query_params['sort_order'] == 'DESC':
            SORT_ORDER = SORT_BY.desc()

        # Default filters
        FILTERS = [
            GameModel.price >= query_params['min_price'],
            GameModel.price <= query_params['max_price']
        ]

        if not current_user:
            FILTERS.append(GameModel.is_hidden == False)
        else:
            if current_user.is_manager():
                FILTERS.append(
                    sqlalchemy.or_(
                        GameModel.managed_by == current_user,
                        GameModel.is_hidden == False
                    )
                )
            elif current_user.is_admin() or current_user.is_superadmin():
                FILTERS.append(
                    sqlalchemy.or_(
                        GameModel.is_hidden == True,
                        GameModel.is_hidden == False
                    )
                )

        if query_params['genres']:
            FILTERS.append(GenreModel.name.in_(query_params['genres']))

        if query_params['title_contains']:
            FILTERS.append(GameModel.title.contains(query_params['title_contains']))

        FILTERED_QUERY = GameModel.query\
            .join(GameModel.genres)\
            .join(UserModel)\
            .filter(*FILTERS)\
            .order_by(SORT_ORDER)\
            .distinct(GameModel.id)

        # amount of games, that match query
        total_games = len(FILTERED_QUERY.all())
        LAST_PAGE = math.ceil(total_games / query_params['items_per_page'])

        if query_params['current_page'] > LAST_PAGE:
            query_params['current_page'] = LAST_PAGE

        PAGINATED_QUERY = FILTERED_QUERY\
            .limit(query_params['items_per_page'])\
            .offset((query_params['current_page'] - 1) * query_params['items_per_page'])\
            .all()

        response = {
            'current_page': query_params['current_page'],
            'last_page': LAST_PAGE,
            'total_games': total_games,
            'items_per_page': query_params['items_per_page'],
            'games': {game.public_id: game for game in PAGINATED_QUERY}
        }

        return games_paginated_list_schema.dump(response)


class HideGameResource(Resource):
    @jwt_required()
    @role_required('Manager', 'Admin', get_current_user=True)
    def get(self, game_id, current_user: UserModel):
        game = GameModel.get_game(game_id)

        if not game:
            return {'message': f'Game Not Found'}, 404

        if not any([game.is_managed_by(current_user), current_user.is_admin(), current_user.is_superadmin()]):
            return {'message': f'Access denied!'}, 401

        if game.is_hidden:
            return {'message': f"Game [{game_id}]->'{game.title}' is already HIDDEN!"}

        game.hide()

        return {
            'message': f"Game {game_id}->'{game.title}' is hidden and is NO MORE VISIBLE for Guests, Users and other Managers!"
        }


class UnhideGameResource(Resource):
    @jwt_required()
    @role_required('Manager', 'Admin', get_current_user=True)
    def get(self, game_id, current_user):
        game = GameModel.get_game(game_id)

        if not game:
            return {'message': f'Game Not Found'}, 404

        if not any([game.is_managed_by(current_user), current_user.is_admin(), current_user.is_superadmin()]):
            return {'message': f'Access denied!'}, 401

        if not game.is_hidden:
            return {'message': f"Game [{game_id}]->'{game.title}' is already UNHIDDEN and IS VISIBLE for EVERYONE!"}

        game.unhide()

        return {
            'message': f"Game [{game_id}]->'{game.title}' is unhidden and IS VISIBLE for EVERYONE!"
        }
