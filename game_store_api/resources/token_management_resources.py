from flask_restful import Resource

# JWT stuff
from flask_jwt_extended import get_jwt
from flask_jwt_extended import jwt_required
from flask_jwt_extended import get_jwt_identity
from flask_jwt_extended import create_access_token
from game_store_api.jwt_blocklist import JWTBlockList


# api/v1/refresh_access_token
class RefreshAccessTokenResource(Resource):
    @jwt_required(refresh=True)
    def post(self):
        current_user = get_jwt_identity()
        access_token = create_access_token(identity=current_user)
        return {'access_token': access_token}


# Does exactly the same as UserLogoutResource | /api/v1/logout
# api/v1/revoke_access_token
class RevokeAccessTokenResource(Resource):
    @jwt_required()
    def post(self):
        jwt_payload = get_jwt()
        JWTBlockList.add_token(jwt_payload)

        return {'message': 'JWT Access Token has been revoked.'}


class RevokeRefreshTokenResource(Resource):
    @jwt_required(refresh=True)
    def post(self):
        jwt_payload = get_jwt()
        JWTBlockList.add_token(jwt_payload)

        return {'message': 'JWT Refresh Token has been revoked.'}
