from flask_restful import Resource
from flask import request

# JWT stuff
from flask_jwt_extended import create_access_token
from flask_jwt_extended import create_refresh_token
from flask_jwt_extended import jwt_required
from flask_jwt_extended import get_jwt
from flask_jwt_extended import get_jwt_identity
from game_store_api.jwt_blocklist import JWTBlockList

# Marshmallow schemas
from game_store_api.marshmallow_schemas import user_data_schema
from game_store_api.marshmallow_schemas import user_registration_schema
from game_store_api.marshmallow_schemas import user_registration_response_schema
from game_store_api.marshmallow_schemas import user_login_schema
from game_store_api.marshmallow_schemas import user_login_response_schema

# Models
from game_store_api.models import db
from game_store_api.models import UserModel


# api/v1/register
class UserRegistrationResource(Resource):
    def post(self):
        data = request.get_json()

        # Validating data structure to match the marshmallow schema
        errors = user_registration_schema.validate(data)
        if errors:
            response = {
                'message': 'User data validation failed.',
                'errors': errors
            }
            return response, 400

        email, username, password = data.get('email'), data.get('username'), data.get('password')
        errors = {}
        if not UserModel.email_is_free(email):
            errors['email'] = f"Email '{email}' is already registered."

        if not UserModel.username_is_free(username):
            errors['username'] = f"User '{username}' already exists."

        password_errors = UserModel.check_password_strength(password)
        if password_errors:
            errors['password'] = password_errors

        if errors:
            response = {
                'message': 'User registration FAILED',
                'errors': errors
            }
            return response, 400

        new_user = UserModel.add_new_user(data)

        if not new_user:
            response = {
                'message': 'User registration FAILED.',
                'errors': {
                    'database': 'User has not been added to database.'
                }
            }
            return response, 500

        registration_response = user_registration_schema.dump(new_user)
        registration_response['access_token'] = create_access_token(identity=username)
        registration_response['refresh_token'] = create_refresh_token(identity=username)

        return user_registration_response_schema.dump(registration_response)


# api/v1/login
class UserLoginResource(Resource):
    def post(self):
        data = request.get_json()

        errors = user_login_schema.validate(data)
        if errors:
            response = {
                'message': 'Login FAILED',
                'errors': errors
            }
            return response, 400

        username, password = data.get('username'), data.get('password')

        user = UserModel.get_user(username)
        if not user:
            response = {
                'message': 'Login FAILED',
                'errors': {
                    'username': f'User \'{username}\' does not exist'
                }
            }
            return response, 400

        if not UserModel.check_password_hash(password, user.password):
            response = {
                'message': 'Login FAILED.',
                'errors': {
                    'password': 'Wrong password.'
                }
            }
            return response, 400

        login_response = user_data_schema.dump(user)
        login_response['access_token'] = create_access_token(identity=username)
        login_response['refresh_token'] = create_refresh_token(identity=username)

        return user_login_response_schema.dump(login_response)


# api/v1/logout
class UserLogoutResource(Resource):
    @jwt_required()
    def post(self):
        username = get_jwt_identity()
        JWTBlockList.add_token(get_jwt())

        return {'message': f'@{username} has been logged out.'}
