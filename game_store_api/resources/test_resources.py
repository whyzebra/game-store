from flask_restful import Resource
from flask_jwt_extended import jwt_required
from flask_jwt_extended import get_jwt_identity
from game_store_api.common import role_required


class TestProtectedResource(Resource):
    @jwt_required()
    @role_required('Admin', 'Manager')
    def get(self):
        identity = get_jwt_identity()
        return {'message': f'{identity}, welcome to protected resource :)'}
