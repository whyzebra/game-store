from flask import Flask
from flask_restful import Api
from flask_jwt_extended import JWTManager
from setup.setup_database_models import setup_test_database
from game_store_api.redis_db import RedisDB
from game_store_api.endpoints import register_all_endpoints


def load_app_config(app, config_type):
    if config_type == 'Development':
        app.config.from_object('game_store_api.config.DevelopmentConfig')

    elif config_type == 'Test':
        app.config.from_object('game_store_api.config.TestConfig')


def init_database(app):
    from game_store_api.models import db
    db.init_app(app)
    return db


def init_marshmallow(app):
    from game_store_api.marshmallow_schemas import ma
    ma.init_app(app)
    return ma


def setup_jwt_blocklist(jwt):
    from game_store_api.jwt_blocklist import JWTBlockList

    @jwt.token_in_blocklist_loader
    def check_if_token_is_blocked(jwt_headers, jwt_payload):
        return JWTBlockList.is_token_blocked(jwt_payload)


def setup_development_game_store_api():
    app = Flask(__name__)
    load_app_config(app, config_type='Development')

    db = init_database(app)
    RedisDB.connect_development_instance()
    app.app_context().push()

    # entry point for creating database if it does not exist
    # setup_test_database()

    api = Api(app)
    jwt = JWTManager(app)
    setup_jwt_blocklist(jwt)

    init_marshmallow(app)
    register_all_endpoints(api)

    @app.before_first_request
    def create_tables():
        db.create_all()

    return app


def setup_test_game_store_api():
    app = Flask(__name__)
    load_app_config(app, config_type='Test')

    db = init_database(app)
    RedisDB.connect_test_instance()
    app.app_context().push()

    # entry point for creating database if it does not exist
    # setup_test_database()

    api = Api(app)
    jwt = JWTManager(app)
    setup_jwt_blocklist(jwt)

    init_marshmallow(app)
    register_all_endpoints(api)

    @app.before_first_request
    def create_tables():
        db.create_all()

    return app
