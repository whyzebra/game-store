from game_store_api.models import UserModel
from flask_jwt_extended import get_jwt_identity
import functools


def role_required(role_title='Admin', *other_role_titles, get_current_user=False):
    """
    Grants access to the Resource only for Users with specified role titles.
    P.S. 'SuperAdmin' User has access to any @role_required decorated resource

    :param role_title: role title with access to the Resource
    :param other_role_titles: any amount of role titles with access to the Resource
    :param get_current_user: if set to True, provides decorated function with additional
                        argument 'user: UserModel' of current user
    :return: decorated function
    """
    def decorator_role_required(decorated_func):
        @functools.wraps(decorated_func)
        def role_required_wrapper(*args, **kwargs):
            username = get_jwt_identity()
            current_user = UserModel.get_user(username=username)

            if current_user.role_title not in [role_title, *other_role_titles, 'SuperAdmin']:
                response = {
                    'message': 'Access Denied!',
                    'errors': {
                        'account_permission': 'Your account does not have permission to reach this resource.'
                    }
                }
                return response, 401

            if get_current_user:
                kwargs['current_user'] = current_user

            return decorated_func(*args, **kwargs)
        return role_required_wrapper
    return decorator_role_required
