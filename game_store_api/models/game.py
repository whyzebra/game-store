from game_store_api.models import db
from game_store_api.short_uuid_generator import generate_short_uuid
from game_store_api.models import game_genres_association_table
from typing import Union


class GameModel(db.Model):
    query: db.Query

    id = db.Column(db.Integer, primary_key=True)
    public_id = db.Column(db.String, unique=True)

    # public fields, available for all users
    title = db.Column(db.String(50), unique=True)
    genres = db.relationship('GenreModel', secondary=game_genres_association_table, back_populates='games')
    developer = db.Column(db.String(50))
    publisher = db.Column(db.String(50))
    description = db.Column(db.Text())
    price = db.Column(db.Float())

    # amount of game licenses, that can be sold
    licenses_num = db.Column(db.Integer(), default=10_000_000)

    # Determines whether game is "visible" for guests and users or not
    # True - not visible (hidden)
    # False - visible for everyone
    is_hidden = db.Column(db.Boolean(), default=False)

    manager_id = db.Column(db.Integer, db.ForeignKey('user_model.id', ondelete='SET NULL'), nullable=True)
    managed_by = db.relationship('UserModel', back_populates='managed_games', passive_deletes=True)

    def __init__(self, **kwargs):
        super(GameModel, self).__init__(**kwargs)
        self.public_id = generate_short_uuid()

    def __repr__(self):
        return f'<Game: {self.title}>'

    def hide(self) -> None:
        self.is_hidden = True
        self.save()

    def unhide(self) -> None:
        self.is_hidden = False
        self.save()

    def is_managed_by(self, user: 'UserModel') -> bool:
        return self.managed_by == user

    @classmethod
    def get_game(cls, game_id: Union[None, str] = None, title: Union[None, str] = None) -> Union['GameModel', None]:
        if game_id:
            return cls.query.filter_by(public_id=game_id).first()

        if title:
            return cls.query.filter_by(title=title).first()

        return None

    @classmethod
    def get_min_price(cls):
        return cls.query.order_by(cls.price.asc()).first().price

    @classmethod
    def get_max_price(cls):
        return cls.query.order_by(cls.price.desc()).first().price

    def save(self) -> None:
        """
        Saves class instance to database
        """
        db.session.add(self)
        db.session.commit()
