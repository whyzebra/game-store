from game_store_api.models import db
from game_store_api.models import game_genres_association_table
from typing import List


class GenreModel(db.Model):
    query: db.Query

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(20), unique=True)
    games = db.relationship("GameModel", secondary=game_genres_association_table, back_populates="genres")

    @classmethod
    def get_genre_names_list(cls) -> List[str]:
        """
        Return a list with all genre names in data base

        :return: list of genre names
        """
        return [genre.name for genre in cls.query.all()]

    def save(self) -> None:
        """
        Saves class instance to database
        """
        db.session.add(self)
        db.session.commit()

    def __repr__(self):
        return f'<Genre: {self.name}>'
