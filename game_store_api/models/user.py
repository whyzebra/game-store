from game_store_api.models import db
from game_store_api.models import RoleModel
from game_store_api.short_uuid_generator import generate_short_uuid
from passlib.hash import pbkdf2_sha256
from datetime import datetime
import re

from typing import Union, List


# TODO:
#   - add docstrings to all methods


class UserModel(db.Model):
    # workaround to make Pycharm autocomplete works
    query: db.Query

    id = db.Column(db.Integer, primary_key=True)
    public_id = db.Column(db.String(), unique=True)

    role_title = db.Column(db.String, db.ForeignKey('role_model.title', ondelete='SET NULL'), nullable=True)
    role = db.relationship('RoleModel', back_populates="members", passive_deletes=True)

    username = db.Column(db.String(20), unique=True)
    password = db.Column(db.String(40))
    email = db.Column(db.String(), unique=True)
    first_name = db.Column(db.String(20))
    last_name = db.Column(db.String(20))
    registration_date = db.Column(db.DateTime)

    # fields only for Users with role 'Manager'
    managed_games = db.relationship('GameModel', back_populates='managed_by')

    def __init__(self, **kwargs):
        self.public_id = generate_short_uuid()
        self.role = kwargs.get('role')
        self.username = kwargs.get('username')
        self.password = self.generate_password_hash(kwargs.get('password'))
        self.email = kwargs.get('email')
        self.first_name = kwargs.get('first_name')
        self.last_name = kwargs.get('last_name')
        self.registration_date = datetime.utcnow()

        if not kwargs.get('role'):
            self.role = RoleModel.get_role('User')

    def __repr__(self):
        return f"<{self.role_title}: {self.username}>"

    @staticmethod
    def check_password_strength(password: str) -> Union[List[str], None]:
        """
        Check whether :password: is "strength"
        Password considered as "strength" if it meets all 5 criteria:
            - minimum length 8 characters
            - at least 1 lowercase letter [a-z]
            - at least 1 uppercase letter [A-Z]
            - at least 1 digit
            - at least 1 special symbol (!@#$%^&*)

        :return: True if :password: is "strength", else - False
        """
        if len(password) not in range(8, 21):
            return ['Password length must be in range [8; 20] characters']

        else:
            password_errors = []

            if not bool(re.match(r'(?=.*\d)', password)):
                password_errors.append('Password must contain at least 1 digit')

            if not bool(re.match(r'(?=.*[a-z])', password)):
                password_errors.append('Password must contain at least 1 lowercase letter')

            if not bool(re.match(r'(?=.*[A-Z])', password)):
                password_errors.append('Password must contain at least 1 uppercase letter')

            if not bool(re.match(r'(?=.*[!@#$%^&*])', password)):
                password_errors.append('Password must contain at least 1 special character (!@#$%^&*)')

            if password_errors:
                return password_errors

    @staticmethod
    def generate_password_hash(password: str) -> str:
        """
        Generate "PBKDF2-HMAC-SHA256" based :password: hash

        :param password: string representation of password to be hashed
        :return: "pbkdf2_sha256" hash of :password: as str
        """
        return pbkdf2_sha256.hash(password)

    @staticmethod
    def check_password_hash(password: str, password_hash: str) -> bool:
        """
        Compares hashes of :password: and User password from data base

        :param password: string representation of pure password
        :param password_hash: user password hash from database
        :return: True if hash of provided :password: equals to User.password, otherwise False
        """
        return pbkdf2_sha256.verify(password, password_hash)

    @staticmethod
    def username_is_free(username: str) -> bool:
        """
        Check whether :username: is present in database

        :param username:
        :return: True if :username: is not present in database, else - False
        """
        return not bool(UserModel.query.filter_by(username=username).first())

    @staticmethod
    def email_is_free(email: str) -> bool:
        """
        Check whether :email: is present in database

        :param email:
        :return: True if :email: is not present n database, else - False
        """
        return not bool(UserModel.query.filter_by(email=email).first())

    @staticmethod
    def add_new_user(data: dict) -> Union['UserModel', bool]:
        try:
            new_user = UserModel(**data)
            new_user.save()
            return new_user
        except Exception as e:
            return False

    @classmethod
    def get_user(cls, username) -> 'UserModel':
        return cls.query.filter_by(username=username).first()

    def is_manager(self):
        return self.role_title == 'Manager'

    def is_admin(self):
        return self.role_title == 'Admin'

    def is_superadmin(self):
        return self.role_title == 'SuperAdmin'

    def save(self) -> None:
        """
        Saves class instance to database
        """
        db.session.add(self)
        db.session.commit()
