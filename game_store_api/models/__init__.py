from flask_sqlalchemy import SQLAlchemy


db = SQLAlchemy()

from .association_tables import game_genres_association_table
from .role import RoleModel
from .user import UserModel
from .game import GameModel
from .genre import GenreModel
