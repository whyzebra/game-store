from game_store_api.models import db
from typing import Type


class RoleModel(db.Model):
    query: db.Query

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(20), unique=True)
    members = db.relationship('UserModel', back_populates='role')

    @classmethod
    def get_role(cls, role_title) -> Type['RoleModel']:
        """
        Always return RoleModel with :role_title:.
        In case there is no record with :role_title: in Data Base,
        this method creates it and commit to DB

        :param role_title: role title, to be found in/added to Data Base
        :return: RoleModel with :role_title:
        """
        some_role = cls.query.filter_by(title=role_title).first()

        if not some_role:
            some_role = RoleModel(title=role_title)
            some_role.save()

        return some_role

    @classmethod
    def get_manager_role(cls) -> Type['RoleModel']:
        """
        Always return 'Manager' RoleModel.
        In case 'Manager' role does not exist, adds it to Data Base.

        :return: 'Manager' RoleModel
        """
        return cls.__get_role('Manager')

    @classmethod
    def get_admin_role(cls) -> Type['RoleModel']:
        """
        Always return 'Admin' RoleModel.
        In case 'Admin' role does not exist, adds it to Data Base.

        :return: 'Admin' RoleModel
        """
        return cls.__get_role('Admin')

    def save(self):
        db.session.add(self)
        db.session.commit()

    def __repr__(self):
        return f'<Role: {self.name}>'
