# All association tables for many-to-many relationships will be stored here
from game_store_api.models import db


game_genres_association_table = db.Table(
    'game_genres_association_table',
    db.Column('game_id', db.Integer, db.ForeignKey('game_model.id')),
    db.Column('genre_id', db.Integer, db.ForeignKey('genre_model.id'))
)
