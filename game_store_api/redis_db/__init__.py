import redis

# REDIS_BLOCKLIST = redis.Redis(host='localhost', port='6379', db=13)
# REDIS_USED_SHORT_UUIDS = redis.Redis(host='localhost', port='6379', db=9)


class RedisDB:
    REDIS_BLOCKLIST = None
    REDIS_USED_SHORT_UUIDS = None

    @classmethod
    def connect_development_instance(cls):
        print('Redis "development" config has been loaded')
        cls.REDIS_BLOCKLIST = redis.Redis(host='localhost', port='6379', db=13)
        cls.REDIS_USED_SHORT_UUIDS = redis.Redis(host='localhost', port='6379', db=9)

    @classmethod
    def connect_test_instance(cls):
        print('Redis "test" config has been loaded')
        cls.REDIS_BLOCKLIST = redis.Redis(host='localhost', port='6380', db=13)
        cls.REDIS_USED_SHORT_UUIDS = redis.Redis(host='localhost', port='6380', db=9)

# REDIS_USED_SHORT_UUID = redis.Redis(db=9)
# the idea is to store all used str(uuid4())[:5] in db to avoid collisions
# these ids will be used only for GameModel.public_id
# program will generate uuid4() and in case of collision
# with id from REDIS_USED_SHORT_UUID it wll regenerate uuid4()
# process will be repeated as many times as needed to avoid collision
