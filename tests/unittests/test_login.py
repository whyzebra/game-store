import unittest
from game_store_api import setup_test_game_store_api
from game_store_api.redis_db import RedisDB
from game_store_api.models import db
from setup.setup_database_models import setup_test_database

from pprint import pprint


class AuthTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        cls.app = setup_test_game_store_api()
        cls.app.app_context().push()
        cls.app = cls.app.test_client()

        cls.user_credentials = {
            'username': 'test_user',
            'password': 'User*01'
        }

    def login_into_test_user_account(self):
        api_response = self.app.post(
            'api/v1/login',
            json=self.user_credentials
        )

        return api_response

    def test_successful_login(self):
        api_response = self.login_into_test_user_account()

        self.assertEqual(200, api_response.status_code)
        self.assertEqual('test_user', api_response.json['username'])
        self.assertIsNotNone(api_response.json['access_token'])
        self.assertIsNotNone(api_response.json['refresh_token'])

    def test_successful_logout(self):
        api_login_response = self.login_into_test_user_account()

        access_token = api_login_response.json['access_token']
        headers = {
            'Authorization': f'Bearer {access_token}'
        }

        api_response = self.app.post(
            'api/v1/logout',
            headers=headers,
        )

        self.assertEqual(200, api_response.status_code)

    def tearDown(self) -> None:
        # db.drop_all()
        # db.session.commit()
        pass


if __name__ == '__main__':
    unittest.main()
