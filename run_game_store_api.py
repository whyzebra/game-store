from game_store_api import setup_development_game_store_api


if __name__ == "__main__":
    game_store = setup_development_game_store_api()
    game_store.app_context().push()
    game_store.run(debug=True)
